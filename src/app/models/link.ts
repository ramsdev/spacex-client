export interface Link {
  mission_patch: string;
  wikipedia:     string;
  article_link:  string;
  flickr_images: string [];
}